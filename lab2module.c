#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/debugfs.h>
#include <linux/fs.h>
#include <linux/seq_file.h>
#include <linux/pid.h>
#include <linux/mm.h>
#include <linux/mm_types.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/dcache.h>
#include <asm/io.h>
#include <asm/fpu/types.h>
#include <asm/processor.h>

#define BUFFER_SIZE 1024

MODULE_LICENSE("GPL");

static struct mutex lock;
static struct dentry *debug_dir;
static struct dentry *debug_file;
static struct task_struct* task = NULL;
static void print_dentry_struct(struct seq_file *file, struct task_struct *task);
static void print_vm_area_struct(struct seq_file *file, struct task_struct *task);
static int print_struct(struct seq_file *file, void *data);

static int open_function(struct inode *inodep, struct file *filep){
   mutex_lock(&lock);
   return 0;
}

static int release_function(struct inode *inodep, struct file *filep){
   mutex_unlock(&lock);
   return 0;
}

static ssize_t write_function(struct file *file, const char __user *buffer, size_t length, loff_t *ptr_offset) {
  char user_data[BUFFER_SIZE];
  unsigned long pid_number;
  copy_from_user(user_data, buffer, length);
  sscanf(user_data, "pid: %ld", &pid_number);
  struct pid* pid = find_get_pid(pid_number); 
  task = get_pid_task(pid, PIDTYPE_PID);
  single_open(file, print_struct, NULL);
  return strlen(user_data);
}

static struct file_operations fops = {
  .open = open_function,
  .read = seq_read,
  .write = write_function,
  .release = release_function
};

static int __init mod_init(void) {
  mutex_init(&lock);
  debug_dir = debugfs_create_dir("lab2", NULL);
  debug_file = debugfs_create_file("lab2file", 0777, debug_dir, NULL, &fops);
  return 0;
}

static void __exit mod_exit(void) {
  debugfs_remove_recursive(debug_dir);
}

static void print_dentry_struct(struct seq_file *file, struct task_struct *task) {
  if (task == NULL) {
    seq_printf(file, "Can't find dentry_struct with this pid\n");
  } else  {
    if (task->mm ==0){
	seq_printf(file, "Can't find dentry_struct with this pid\n");
    }else{
        seq_printf(file, "dentry structure: {\n");
	seq_printf(file, "  count: %u,\n", task->mm->mmap->vm_file->f_path.dentry->d_lockref.count);
	seq_printf(file, "  d_time: %u,\n", task->mm->mmap->vm_file->f_path.dentry->d_time);
	seq_printf(file, "  inode_number: %u,\n", task->mm->mmap->vm_file->f_path.dentry->d_inode->i_ino);
	seq_printf(file, "  unhashed: %s,\n", d_unhashed(task->mm->mmap->vm_file->f_path.dentry) ? "true" : "false");
	seq_printf(file, "  unlinked: %s,\n", d_unlinked(task->mm->mmap->vm_file->f_path.dentry) ? "true" : "false");
	seq_printf(file, "}\n");
     }
    } 	  
}

static void print_vm_area_struct(struct seq_file *file, struct task_struct *task) {
  if (task == NULL) {
    seq_printf(file, "Can't find vm_area_struct with this pid\n");
  } else  {
    if (task->mm ==0){
	seq_printf(file, "Can't find vm_area_Struct with this pid\n");
    }else{
        seq_printf(file, "vm area struct\n");
        struct vm_area_struct *pos = NULL;
        seq_printf(file,"%s\t%12s\t%20s\t%5s\t%21s\t%20s\n","address","root","subtree_gap(B)","prot","offset(B) ","mapping");
	for(pos = task->mm->mmap;pos;pos = pos->vm_next){
		seq_printf(file,"0x%hx-0x%hx\t",pos->vm_start,pos->vm_end);
		if (pos->vm_flags & VM_READ){
			seq_printf(file,"r");
		}else{
			seq_printf(file,"-");
		}
		if (pos->vm_flags & VM_WRITE){
			seq_printf(file,"w");

		}else{
			seq_printf(file,"-");
		}
		if (pos->vm_flags & VM_EXEC){
			seq_printf(file,"x");
		}else{
			seq_printf(file,"-");
		}
		if (pos->vm_flags & VM_SHARED){
			seq_printf(file,"s");
		}else{
			seq_printf(file,"-");
		}
		seq_printf(file,"\t%20d",pos->rb_subtree_gap);
		seq_printf(file,"\t%5d",pos->vm_page_prot);	
		seq_printf(file,"\t%21d",pos->vm_pgoff);
		if (pos->vm_file == NULL){
		  seq_printf(file,"\t%20s",pos->vm_file);
		}else {
		  seq_printf(file,"\t%20s",pos->vm_file->f_path.dentry->d_name.name);
		}
		seq_printf(file,"\n");
	}
    } 	  
  }
}
static int print_struct(struct seq_file *file, void *data) {
  print_dentry_struct(file, task);
  print_vm_area_struct(file, task);
  return 0;
}


module_init(mod_init);
module_exit(mod_exit);
